#include "authdialog.h"
#include "ui_authdialog.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>
#include <QDebug>
#include <QTcpSocket>

#include <QFile>

AuthDialog::AuthDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthDialog)
{
    ui->setupUi(this);
}

AuthDialog::~AuthDialog()
{
    delete ui;
}

UserData AuthDialog::GetInputData()
{
    return user;
}

void AuthDialog::on_pushButton_clicked()
{
    QString login = ui->lineEdit->text();
    QString password = ui->lineEdit_2->text();

    user.SetLogin(login);
    user.SetPassword(password);

    this->accept();
}


void AuthDialog::on_AuthDialog_rejected()
{
    exit(EXIT_SUCCESS);
}
