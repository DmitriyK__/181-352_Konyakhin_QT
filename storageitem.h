#ifndef STORAGEITEM_H
#define STORAGEITEM_H

#include <QString>

class StorageItem
{
public:
    StorageItem();
    StorageItem(QString title, QString supplier, QString reciver, QString keeper);

    QString GetTitle() const;
    QString GetSupplier() const;
    QString GetReciever() const;
    QString GetKeeper() const;

    void SetTitle(QString title);
    void SetSupplier(QString supplier);
    void SetReciever(QString reciever);
    void SetKeeper(QString keeper);

private:
    QString title;
    QString supplier;
    QString reciever;
    QString keeper;
};

#endif // STORAGEITEM_H
