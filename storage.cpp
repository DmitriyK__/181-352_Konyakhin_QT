#include "storage.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>

Storage::Storage()
{

}

void Storage::SetFilePath(QString filePath)
{
    this->filePath = filePath;
}

bool Storage::UpdateItem(int index, StorageItem editedItem)
{
    if (index > db.count() - 1) return false;
    db[index] = editedItem;
    return true;
}

void Storage::AddItem(StorageItem item)
{
    db.push_back(item);
}

void Storage::Load()
{
    // Считываем файл json

    QString val; // Содержимое файла
    QFile file; // Файл
    file.setFileName(filePath); // Путь к файлу

    if (file.open(QIODevice::ReadOnly | QIODevice::Text) == true) { // Открываем файл
        val = file.readAll(); // Копируем содержимое файла
        file.close(); // Закрываем файл
    } else { // Если не удалось открыть файл, то выводим сообщение о ошибке
        QMessageBox msg;
        msg.setText("Не удалось прочитать файл с базой склада!");
        msg.exec();
        exit(EXIT_FAILURE); // Выход из приложения
    }

    QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8()); // Парсинг JSON документа
    QJsonObject obj = doc.object(); // Получаем JSON объект из распарсенного документа
    QJsonArray jItems = obj.take("items").toArray(); // Берем массив товаров на складе items из файла

    for (auto jItem : jItems) { // Считываем данные каждый товар в файле
        QString title, supplier, reciever, keeper;

        title = jItem.toObject().take("title").toString(); // получаем название товара
        supplier = jItem.toObject().take("supplier").toString(); // поставщика
        reciever = jItem.toObject().take("reciever").toString(); // получателя
        keeper = jItem.toObject().take("keeper").toString(); // получателя


        StorageItem item(title, supplier, reciever, keeper); // Создаем товар

        db.push_back(item); // Помещаем его в локальную БД
    }
}

void Storage::Save()
{
    QFile file; // Файл
    file.setFileName(filePath); // Путь к файлу

    QJsonDocument doc; // Создаем JSON документ
    QJsonObject obj = doc.object(); // Получаем документ в виде объекта
    QJsonArray arr; // Создаем JSON массив

    for (auto Item : db) { // Для каждого товара на складе в локальной БД
        QJsonObject jItem; // Создаем JSON объект товара

        jItem.insert("reciever", Item.GetReciever()); // получателя
        jItem.insert("supplier", Item.GetSupplier()); // поставщика
        jItem.insert("title", Item.GetTitle()); // записываем поле название товара
        jItem.insert("keeper", Item.GetKeeper()); // записываем поле кладовщика

        arr.push_back(jItem); // помещаем в массив
    }

    obj.insert("items", arr); // помещаем массив в JSON документ под именем items
    doc.setObject(obj); // добавляем объект в документ

    if (file.open(QIODevice::WriteOnly | QIODevice::Text) == true) { // Открываем файл
        file.write(doc.toJson()); // Преобразовываем JSON документ в текст и записываем в файл
        file.close(); // Закрываем файл
    } else { // Если не удалось открыть файл, то выводим ошибку
        QMessageBox msg;
        msg.setText("Не удалось записать файл с базой товаров на складе!");
        msg.exec();
        exit(EXIT_FAILURE); // Выход из приложения
    }
}

QList<StorageItem> Storage::GetItems() const
{
    return db;
}
