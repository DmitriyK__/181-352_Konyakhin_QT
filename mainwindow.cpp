#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "authdialog.h"
#include "additemdialog.h"
#include "adduserdialog.h"
#include <QMessageBox>
#include <QTcpSocket>
#include <QHostAddress>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "qaesencryption.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    AuthDialog dlg(this); // Создаем окошко авторизации
    bool authComplete = false; // Флаг свидетельствующий о успешной авторизации
    socket = new QTcpSocket(); // Создаем сокет для клиента

    while (!authComplete) { // Выполняем цикл, пока пользователь не прошел авторизацию
        dlg.exec(); // Показываем окошко авторизации

        QString login = dlg.GetInputData().GetLogin(); // Берем логин, который ввел пользователь
        QString pass = dlg.GetInputData().GetPassword(); // Берем пароль, который ввел пользователь

        socket->close(); // Закрываем все соединения
        socket->connectToHost(QHostAddress("127.0.0.1"), 6000); // Подключаемся к серверу
        socket->waitForConnected(); // Ждем соединения

        if (socket->state() != QAbstractSocket::ConnectedState) { // Если сервер
            QMessageBox msg;
            msg.setText("Не удалось подключиться к серверу, попробуйте еще раз");
            msg.exec();
            continue; // Выполняем цикл заново
        }

        QByteArray request, response; // Сырые данные для запроса и ответа

        QJsonDocument docRequest; // Создаем содержимое запроса
        QJsonObject obj = docRequest.object(); // Получаем документ в виде объекта

        QAESEncryption encryption(QAESEncryption::AES_128, QAESEncryption::ECB); // Объект для шифрования
        QByteArray key; // Ключ шифрования
        key.fill('a', 128); // 128-битный ключ (можно задать по-другому)

        QJsonObject data; // Объект с данными логина и пароля

        QByteArray encodedText = encryption.encode(login.toUtf8(), key); // Шифрование пароля
        data.insert("login", encodedText.data()); // заполняем логин в запрос

        encodedText = encryption.encode(pass.toUtf8() + login.toUtf8(), key); // Шифрование пароля
        data.insert("pass", encodedText.data()); // заполняем зашифрованный пароль

        obj.insert("type", "auth"); // указываем тип запроса: auth
        obj.insert("data", data); // Заполняем свойство с данными

        docRequest.setObject(obj); // добавляем объект в документ
        request = docRequest.toJson(); // получаем содержимое в виде набора байтов

        if (socket->write(request) >= 0) { // Отправляем запрос на сервер
            if (!socket->waitForReadyRead(5000)) { // Ждем ответ
                QMessageBox msg; // если не пришел ответ, то выводим ошибку
                msg.setText("Сервер не отвечает! Попробуйте позже!");
                msg.exec();
                continue; // Выполняем цикл заново
            } else { // Если ответ пришел
                response = socket->readAll(); // то считываем данные

                QJsonDocument docResponse = QJsonDocument::fromJson(response); // Создаем содержимое запроса
                obj = docResponse.object(); // Получаем документ в виде объекта

                if (obj.find("result")->toString() != "success") { // Если результат НЕ успешен
                    QMessageBox msg; // то выводим ошибку
                    msg.setText("Неверный логин или пароль!");
                    msg.exec();
                    continue; // Выполняем цикл заново
                } else { // Иначе авторизация пройдена!
                    authComplete = true; // Меняем флаг
                    activeUser = UserData(login, pass, obj.find("permission")->toInt()); // Запоминаем сессию
                }
            }
        }
    }

    ui->setupUi(this); // Инициализация интерфейса

    connect(ui->pushButton, SIGNAL (clicked()), this, SLOT (on_pushButton_clicked())); // Вешаем обработчик событий на кнопку
    connect(ui->pushButton_2, SIGNAL (clicked()), this, SLOT (on_pushButton_2_clicked())); // Вешаем обработчик событий на кнопку
    connect(ui->pushButton_5, SIGNAL (clicked()), this, SLOT (on_pushButton_5_clicked())); // Вешаем обработчик событий на кнопку

    ui->label->setText("👤 Логин: " + activeUser.GetLogin()); // Выводим логин в окошко
    QString permissionText; // Определяем права пользователя
    switch (activeUser.GetPermission()) {
        case 2:
            permissionText = "Администратор";
        break;
        case 1:
            permissionText = "Редактирование";
        break;
        case 0:
            permissionText = "Только просмотр";
        break;
    }
    ui->label_2->setText("🔑 Уровень прав: " + permissionText); // Выводим права пользователя в окошко

    ////////////////////////
    // ЗАПОЛНЕНИЕ ТАБЛИЦЫ:
    ////////////////////////

    // 1. Получение данных с сервера
    QByteArray request, response; // Сырые данные для запроса и ответа
    QJsonDocument docRequest; // Создаем содержимое запроса
    QJsonObject obj = docRequest.object(); // Получаем документ в виде объекта

    obj.insert("type", "getData"); // указываем тип запроса
    docRequest.setObject(obj);

    request = docRequest.toJson(); // Преобразлвание данных json в набор байтов

    if (socket->write(request) >= 0) { // Отправляем запрос на сервер
        if (!socket->waitForReadyRead(1000)) { // Ждем ответ
            QMessageBox msg; // если не пришел ответ, то выводим ошибку
            msg.setText("Сервер не отвечает! Попробуйте позже!");
            msg.exec();
        } else { // Если ответ пришел
            response = socket->readAll(); // то считываем данные

            QJsonDocument docResponse = QJsonDocument::fromJson(response); // Получаем содержимое ответа

            for (auto jItem : docResponse.object().find("data")->toArray()) { // Считываем данные каждый товар в файле
                QString title, supplier, reciever, keeper;

                title = jItem.toObject().take("title").toString(); // получаем название товара
                supplier = jItem.toObject().take("supplier").toString(); // поставщика
                reciever = jItem.toObject().take("reciever").toString(); // получателя
                keeper = jItem.toObject().take("keeper").toString(); // получателя

                StorageItem item(title, supplier, reciever, keeper); // Создаем товар
                storageDB.AddItem(item); // Добавляем товар в локальную БД
            }
        }
    }

    // 2. Визуальное заполнение таблицы
    QList<StorageItem> items = storageDB.GetItems();
    for (auto item : items) {
        ui->tableWidget->insertRow (ui->tableWidget->rowCount());
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 0, new QTableWidgetItem(item.GetTitle()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 1, new QTableWidgetItem(item.GetSupplier()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 2, new QTableWidgetItem(item.GetReciever()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 3, new QTableWidgetItem(item.GetKeeper()));

    }

    // 3. Растягиваем таблицу по ширине
    for (int c = 0; c < ui->tableWidget->horizontalHeader()->count(); ++c)
    {
        ui->tableWidget->horizontalHeader()->setSectionResizeMode(
            c, QHeaderView::Stretch);
    }

    if (activeUser.GetPermission() == 0) { // Если права не позволяют редактировать данные
        ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers); // То ограничиваем редактирование таблицы
        ui->pushButton_2->hide();
        ui->pushButton_3->hide();
        ui->pushButton_4->hide();
    } else if (activeUser.GetPermission() == 1) {
        ui->pushButton_3->hide();
        ui->pushButton_4->hide();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked() // Смена пароля
{
    QByteArray request, response;

    QJsonDocument docRequest; // Создаем содержимое запроса
    QJsonObject obj = docRequest.object(); // Получаем документ в виде объекта

    QAESEncryption encryption(QAESEncryption::AES_128, QAESEncryption::ECB); // Объект для шифрования
    QByteArray key; // Ключ шифрования
    key.fill('a', 128); // 128-битный ключ (можно задать по-другому)

    obj.insert("type", "changePassword"); // указываем тип запроса

    QByteArray encodedText = encryption.encode(activeUser.GetLogin().toUtf8(), key); // Шифрование пароля

    obj.insert("login", encodedText.data()); // заполняем логин в запрос

    encodedText = encryption.encode(ui->lineEdit->text().toUtf8() + activeUser.GetLogin().toUtf8(), key); // Шифрование пароля

    obj.insert("newPass", encodedText.data()); // заполняем пароль в запрос
    docRequest.setObject(obj);

    request = docRequest.toJson();

    if (socket->write(request) >= 0) { // Отправляем запрос на сервер
        if (!socket->waitForReadyRead(1000)) { // Ждем ответ
            QMessageBox msg; // если не пришел ответ, то выводим ошибку
            msg.setText("Сервер не отвечает! Попробуйте позже!");
            msg.exec();
        } else { // Если ответ пришел
            response = socket->readAll(); // то считываем данные

            if (QString::fromUtf8(response) != "success") { // Если результат НЕ успешен
                QMessageBox msg; // то выводим ошибку
                msg.setText("Не удалось поменять пароль!");
                msg.exec();
            } else { // Иначе пароль изменен!
                QMessageBox msg;
                msg.setText("Пароль изменен!");
                msg.exec();
            }
        }
    }
}

void MainWindow::on_pushButton_2_clicked() // Сохранение изменений в таблице
{
    QByteArray request, response;

    QJsonDocument docRequest; // Создаем содержимое запроса
    QJsonObject obj = docRequest.object(); // Получаем документ в виде объекта

    obj.insert("type", "updateData"); // указываем тип запроса
    QJsonArray arr;

    for (int i = 0; i < ui->tableWidget->verticalHeader()->count(); i++) { // Для каждой строчки в таблицу
        StorageItem editedItem(ui->tableWidget->item(i, 0)->text(), ui->tableWidget->item(i, 1)->text(), ui->tableWidget->item(i, 2)->text(), ui->tableWidget->item(i, 3)->text());

        QJsonObject jItem;
        // Заполняем обновленные данные
        jItem.insert("reciever", editedItem.GetReciever()); // получателя
        jItem.insert("supplier", editedItem.GetSupplier()); // поставщика
        jItem.insert("title", editedItem.GetTitle()); // записываем поле название товара
        QString keep = editedItem.GetKeeper();
        keep[0] = keep[0].toUpper();
        jItem.insert("keeper", keep); // записываем поле кладовщика

        arr.append(jItem);
    }

    obj.insert("data", arr);
    docRequest.setObject(obj);

    request = docRequest.toJson();

    if (socket->write(request) >= 0) { // Отправляем запрос на сервер
        if (!socket->waitForReadyRead(10000)) { // Ждем ответ
            QMessageBox msg; // если не пришел ответ, то выводим ошибку
            msg.setText("Сервер не отвечает! Попробуйте позже!");
            msg.exec();
        } else { // Если ответ пришел
            response = socket->readAll(); // то считываем данные

            if (QString::fromUtf8(response) != "success") { // Если результат НЕ успешен
                QMessageBox msg; // то выводим ошибку
                msg.setText("Не удалось сохранить данные!");
                msg.exec();
            } else { // Иначе данные сохранения
                QMessageBox msg;
                msg.setText("Данные сохранены!");
                msg.exec();
            }
        }
    }


    //storageDB.Save();
}

void MainWindow::on_pushButton_3_clicked() // Добавление товара
{
    AddItemDialog dlg(this); // Вызываем диалог для добавления товара
    if (dlg.exec() == QDialog::Accepted) { // Если данные введены то заполняем данные в таблицу
        ui->tableWidget->insertRow (ui->tableWidget->rowCount());
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 0, new QTableWidgetItem(dlg.GetInputData().GetTitle()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 1, new QTableWidgetItem(dlg.GetInputData().GetSupplier()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 2, new QTableWidgetItem(dlg.GetInputData().GetReciever()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 3, new QTableWidgetItem(dlg.GetInputData().GetKeeper()));
        storageDB.AddItem(dlg.GetInputData()); // Добавляем товар в локальную БД
    }
}

void MainWindow::on_pushButton_4_clicked() // Добавление пользователя
{
    AddUserDialog dlg(this); // Вызываем диалоговое окно
    if (dlg.exec() == QDialog::Accepted) { // Если данные введены
        QByteArray request, response; // создаем данные для запроса и ответ а

        // Добавляем пользователя
        QJsonDocument docRequest; // Создаем содержимое запроса
        QJsonObject obj = docRequest.object(); // Получаем документ в виде объекта

        QAESEncryption encryption(QAESEncryption::AES_128, QAESEncryption::ECB); // Объект для шифрования
        QByteArray key; // Ключ шифрования
        key.fill('a', 128); // 128-битный ключ (можно задать по-другому)

        obj.insert("type", "addUser"); // указываем тип запроса
        QByteArray encodedText = encryption.encode(dlg.GetInputData().GetLogin().toUtf8(), key); // Шифрование пароля
        obj.insert("login", encodedText.data()); // заполняем логин в запрос
        encodedText = encryption.encode(dlg.GetInputData().GetPassword().toUtf8() + dlg.GetInputData().GetLogin().toUtf8(), key); // Шифрование пароля
        obj.insert("pass", encodedText.data()); // заполняем пароль в запрос
        obj.insert("permission",  dlg.GetInputData().GetPermission()); // заполняем уровень прав в запрос
        docRequest.setObject(obj);

        request = docRequest.toJson(); // Сохраняем содержимое в виде набора байтов

        if (socket->write(request) >= 0) { // Отправляем запрос на сервер
            if (!socket->waitForReadyRead(1000)) { // Ждем ответ
                QMessageBox msg; // если не пришел ответ, то выводим ошибку
                msg.setText("Сервер не отвечает! Попробуйте позже!");
                msg.exec();
            } else { // Если ответ пришел
                response = socket->readAll(); // то считываем данные

                if (QString::fromUtf8(response) != "success") { // Если результат НЕ успешен
                    QMessageBox msg; // то выводим ошибку
                    msg.setText("Не удалось добавить пользователя!");
                    msg.exec();
                } else { // Иначе пользователь добавлен
                    QMessageBox msg;
                    msg.setText("Пользователь " + dlg.GetInputData().GetLogin() + " добавлен!");
                    msg.exec();
                }
            }
        }
    }
}

void MainWindow::on_pushButton_5_clicked() // Поиск по таблице
{
    // Сначала раскрываем все
    for (int i = 0; i < ui->tableWidget->verticalHeader()->count(); i++) {
        ui->tableWidget->showRow(i);
    }

    bool contain = false;
    // Потом скрываем все те, которые не соответсвуют запросу
    for (int i = 0; i < ui->tableWidget->verticalHeader()->count(); i++) {
        if (ui->tableWidget->item(i, 0)->text().contains(ui->lineEdit_2->text())) {
             contain = true;
        } else if (ui->tableWidget->item(i, 1)->text().contains(ui->lineEdit_2->text())) {
            contain = true;
        } else if (ui->tableWidget->item(i, 2)->text().contains(ui->lineEdit_2->text())) {
            contain = true;
        } else if (ui->tableWidget->item(i, 3)->text().contains(ui->lineEdit_2->text())) {
            contain = true;
        } else {
            contain = false;
        }

        if (!contain) ui->tableWidget->hideRow(i);

        contain = false;
    }
}



