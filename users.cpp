#include "users.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>

Users::Users()
{

}

Users::Users(QString filePath)
{
    this->filePath = filePath;
    Load();
}

void Users::Load()
{
    // Считываем файл json

    QString val; // Содержимое файла
    QFile file; // Файл
    file.setFileName(filePath); // Путь к файлу

    if (file.open(QIODevice::ReadOnly | QIODevice::Text) == true) { // Открываем файл
        val = file.readAll(); // Копируем содержимое файла
        file.close(); // Закрываем файл
    } else { // Если не удалось открыть файл, то выводим сообщение о ошибке
        QMessageBox msg;
        msg.setText("Не удалось прочитать файл с базой пользователей!");
        msg.exec();
        exit(EXIT_FAILURE); // Выход из приложения
    }

    QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8()); // Парсинг JSON документа
    QJsonObject obj = doc.object(); // Получаем JSON объект из распарсенного документа
    QJsonArray jUsers = obj.take("users").toArray(); // Берем массив пользователей users из файла

    for (auto jUser : jUsers) { // Считываем данные каждого пользователя в файле
        QString login, password; int permission;

        login = jUser.toObject().take("login").toString(); // получаем логин
        password = jUser.toObject().take("password").toString(); // пароль
        permission = jUser.toObject().take("permission").toInt(); // права

        UserData user(login, password, permission); // Создаем пользователя

        db.push_back(user); // Помещаем его в локальную БД
    }
}

void Users::Save()
{
    QFile file; // Файл
    file.setFileName(filePath); // Путь к файлу

    QJsonDocument doc; // Создаем JSON документ
    QJsonObject obj = doc.object(); // Получаем документ в виде объекта
    QJsonArray arr; // Создаем JSON массив

    for (auto User : db) { // Для каждого пользователя в локальной БД
        QJsonObject jUser; // Создаем JSON объект пользователя

        jUser.insert("login", User.GetLogin()); // записываем поле логин
        jUser.insert("password", User.GetPassword()); // пароль
        jUser.insert("permission", User.GetPermission()); // права

        arr.push_back(jUser); // помещаем в массив
    }

    obj.insert("users", arr); // помещаем массив в JSON документ под именем users
    doc.setObject(obj); // добавляем объект в документ

    if (file.open(QIODevice::WriteOnly | QIODevice::Text) == true) { // Открываем файл
        file.write(doc.toJson()); // Преобразовываем JSON документ в текст и записываем в файл
        file.close(); // Закрываем файл
    } else { // Если не удалось открыть файл, то выводим ошибку
        QMessageBox msg;
        msg.setText("Не удалось записать файл с базой пользователей!");
        msg.exec();
        exit(EXIT_FAILURE); // Выход из приложения
    }
}

bool Users::FindUser(QString login, UserData &user)
{
    for (auto User : db) { // Для каждого пользователя в локальной БД
        if (User.GetLogin() == login) { // Если логин совпадает
            user = User; // то записываем в переменную искомого пользователя
            return true; // Возвращаем true
        }
    }

    return false; // Если ничего не нашли то false
}

bool Users::ChangePassword(QString login, QString newPassword)
{
    for (int i = 0; i < db.size(); i++) { // Для каждого пользователя в локальной БД
        if (db[i].GetLogin() == login) { // Если логин совпадает
            db[i].SetPassword(newPassword); // то меняем ему пароль на новый
            return true; // Возвращаем true
        }
    }
    return false; // Если ничего не нашли то false
}

void Users::SetFilePath(QString filePath)
{
    this->filePath = filePath; // Перезаписываем переменную
}

void Users::AddUser(UserData userData)
{
    db.push_back(userData); // Добавляем юзера в конец списка
}
