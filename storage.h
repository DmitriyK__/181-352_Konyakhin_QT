#ifndef STORAGE_H
#define STORAGE_H

#include <QString>
#include <QList>
#include <storageitem.h>

class Storage
{
public:
    Storage();

    void SetFilePath(QString filePath);
    bool UpdateItem(int index, StorageItem editedItem);
    void AddItem(StorageItem item);

    void Load();
    void Save();

    QList<StorageItem> GetItems() const;

private:
    QList<StorageItem> db;
    QString filePath;
};

#endif // STORAGE_H
