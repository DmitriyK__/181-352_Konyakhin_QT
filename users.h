#ifndef USERS_H
#define USERS_H

#include <QString>
#include <QList>
#include "userdata.h"

class Users // БД пользователей
{
public:
    Users();
    Users(QString filePath); // Развернуть базу данных из файла

    void Load(); // Загрузить базу данных из файла
    void Save(); // Сохранить все изменения в файл

    bool FindUser(QString login, UserData &user); // Найти пользователя
    bool ChangePassword(QString login, QString newPassword); // Сменить пароль

    void SetFilePath(QString filePath); // Указать путь к файлу
    void AddUser(UserData userData); // Добавить пользователя

private:
    QString filePath; // Путь к файлу
    QList<UserData> db; // Локальная база данных внутри оперативной памяти
};

#endif // USERS_H
