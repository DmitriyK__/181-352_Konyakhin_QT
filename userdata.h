#ifndef USERDATA_H
#define USERDATA_H

#include <QString>

class UserData // Класс данных
{
public:
    UserData();
    UserData(QString, QString, int);

    QString GetLogin() const;
    QString GetPassword() const;
    int GetPermission() const;

    void SetLogin(QString login);
    void SetPassword(QString pass);
    void SetPermission(int permission);

private:
    QString login; // Логин
    QString password; // Пароль
    int permission; // Права доступа
};

#endif // USERDATA_H
