#include "storageitem.h"

StorageItem::StorageItem()
{

}

StorageItem::StorageItem(QString title, QString supplier, QString reciver, QString keeper)
{
    this->title = title;
    this->supplier = supplier;
    this->reciever = reciver;
    this->keeper = keeper;
}

QString StorageItem::GetTitle() const
{
    return title;
}

QString StorageItem::GetSupplier() const
{
    return supplier;
}

QString StorageItem::GetReciever() const
{
    return reciever;
}

QString StorageItem::GetKeeper() const
{
    return keeper;
}

void StorageItem::SetTitle(QString title)
{
    this->title = title;
}

void StorageItem::SetSupplier(QString supplier)
{
    this->supplier = supplier;
}

void StorageItem::SetReciever(QString reciever)
{
    this->reciever = reciever;
}

void StorageItem::SetKeeper(QString keeper)
{
    this->keeper = keeper;
}
